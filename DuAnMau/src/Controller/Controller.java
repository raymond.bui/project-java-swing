/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entities.ACCOUNT;
import Entities.info;
import View.Inside;
import View.Inside1;
import View.Inside2;
import View.Loading;
import View.Login;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author raymo
 */
public class Controller extends DAO.DAO {

    private String Username;
    private String Password;
    private static ACCOUNT account;
    private static info info;
    private static View.Loading loading;
    private static View.Login login;
    private static View.Inside inside;
    private static View.Inside1 inside1;
    private static View.Inside2 inside2;

    public static Loading getLoading() {
        return loading;
    }

    public static void setLoading(Loading loading) {
        Controller.loading = loading;
    }

    public static Login getLogin() {
        return login;
    }

    public static void setLogin(Login login) {
        Controller.login = login;
    }

    public static Inside getInside() {
        return inside;
    }

    public static void setInside(Inside inside) {
        Controller.inside = inside;
    }

    public info getInfo() {
        return info;
    }

    public void setInfo(info info) {
        this.info = info;
    }
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ACCOUNT getAccount() {
        return account;
    }

    public void setAccount(ACCOUNT account) {
        this.account = account;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public static Inside1 getInside1() {
        return inside1;
    }

    public static void setInside1(Inside1 inside1) {
        Controller.inside1 = inside1;
    }

    public static Inside2 getInside2() {
        return inside2;
    }

    public static void setInside2(Inside2 inside2) {
        Controller.inside2 = inside2;
    }

    public void checklogin() {

        String sql = "Select * from ACCOUNT where USERNAME=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, Username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                account.setUSERNAME(rs.getString("USERNAME"));
                account.setROLE(rs.getInt("ROLE"));
                account.setPASSWORD(rs.getString("PASSWORD"));

                //
                if (Password.equals(account.getPASSWORD())) {

                    System.out.println(Username);
                    message = "Login Success";

                } else {
                    message = "Login Failed";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void loginSuccess() {
        loading.setVisible(false);
        login.setVisible(false);
        System.out.println(account.getUSERNAME());
        inside.setAccount(account);
        inside1.setAccount(account);
        inside2.setAccount(account);
        if (account.getROLE() == 0) {
            inside.setTypelogin(account.getROLE());
            inside.setVisible(true);
        }
        if (account.getROLE() == 1) {
            inside1.setTypelogin(account.getROLE());
            inside1.setVisible(true);
        }
        if (account.getROLE() == 2) {
            inside2.setTypelogin(account.getROLE());
            inside2.setVisible(true);
        }

    }

   

    public void runloading() {
        loading.setVisible(true);
    }

    public void runlogin() {
        loading.setVisible(false);
        login.setVisible(true);
    }
}
