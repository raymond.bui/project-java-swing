/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Bill;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class BillDAO {

    Connection con = Connector.connect();

    public ArrayList listBill() {
        String stm = "SELECT * FROM BILL";
        ArrayList<Bill> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Bill bill = new Bill();
                bill.setId(rs.getInt(1));
                bill.setName(rs.getString(2));
                bill.setTelephone(rs.getInt(3));
                bill.setAddress(rs.getString(4));
                bill.setDate(rs.getString(5));
                list.add(bill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Bill getBillByID(int id) {
        Bill bill = new Bill();
        String stm = "SELECT * FROM BILL WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                bill.setId(rs.getInt(1));
                bill.setName(rs.getString(2));
                bill.setTelephone(rs.getInt(3));
                bill.setAddress(rs.getString(4));
                bill.setDate(rs.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return bill;
    }

    public Bill addBill(Bill bill) {
        String stm = "INSERT INTO BILL(NAME, TELEPHONE, ADDRESS, DATE) VALUES(?, ?, ?, ?) SELECT @@IDENTITY asLastID";
        int newId = -1;
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, bill.getName());
            ps.setInt(2, bill.getTelephone());
            ps.setString(3, bill.getAddress());
            ps.setString(4, bill.getDate());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                newId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        Bill newBill = getBillByID(newId);
        return newBill;
    }
    
    public void updateBill(Bill bill, String name, int tel, String address, String date) {
        String stm = "UPDATE BILL SET NAME = ?, TELEPHONE = ?, ADDRESS = ?, DATE = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, tel);
            ps.setString(3, address);
            ps.setString(4, date);
            ps.setInt(5, bill.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateBillByID(int id, String name, int tel, String address, String date) {
        String stm = "UPDATE CUSTOMER SET USERNAME = ?, PASSWORD = ?, RANK = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, tel);
            ps.setString(3, address);
            ps.setString(4, date);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteBill(Bill bill) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, bill.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteBillByID(int id) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
