/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Entities.Category;

/**
 *
 * @author Simon
 */
public class CategoryDAO {

    Connection con = Connector.connect();

    public Category getCategoryByID(int id) {
        Category cat = new Category();
        String stm = "SELECT * FROM CATEGORY WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cat.setId(rs.getInt(1));
                cat.setType(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return cat;
    }
}
