/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Customer;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class CustomerDAO {

    Connection con = Connector.connect();

    public ArrayList listCustomer() {
        String stm = "SELECT * FROM CUSTOMER";
        ArrayList<Customer> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Customer cs = new Customer();
                cs.setId(rs.getInt(1));
                cs.setUsername(rs.getString(2));
                cs.setPassword(rs.getString(3));
                cs.setRank(new RankDAO().getRankByID(rs.getInt(4)));
                cs.setName(rs.getString(5));
                cs.setTelephone(rs.getInt(6));
                cs.setAddress(rs.getString(7));
                list.add(cs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Customer getCustomerByID(int id) {
        Customer cs = new Customer();
        String stm = "SELECT * FROM CUSTOMER WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cs.setId(rs.getInt(1));
                cs.setUsername(rs.getString(2));
                cs.setPassword(rs.getString(3));
                cs.setRank(new RankDAO().getRankByID(rs.getInt(4)));
                cs.setName(rs.getString(5));
                cs.setTelephone(rs.getInt(6));
                cs.setAddress(rs.getString(7));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return cs;
    }

    public void addCustomer(Customer em) {
        String stm = "INSERT INTO CUSTOMER(USERNAME, PASSWORD, RANK, NAME, TELEPHONE, ADDRESS) VALUES(?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, em.getUsername());
            ps.setString(2, em.getPassword());
            ps.setInt(3, em.getRank().getId());
            ps.setString(4, em.getName());
            ps.setInt(5, em.getTelephone());
            ps.setString(6, em.getAddress());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void updateCustomer(Customer em, String un, String pw, int rank, String name, int tel, String address) {
        String stm = "UPDATE CUSTOMER SET USERNAME = ?, PASSWORD = ?, RANK = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, un);
            ps.setString(2, pw);
            ps.setInt(3, rank);
            ps.setString(4, name);
            ps.setInt(5, tel);
            ps.setString(6, address);
            ps.setInt(7, em.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void updateCustomerByID(int id, String un, String pw, int rank, String name, int tel, String address) {
        String stm = "UPDATE CUSTOMER SET USERNAME = ?, PASSWORD = ?, RANK = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, un);
            ps.setString(2, pw);
            ps.setInt(3, rank);
            ps.setString(4, name);
            ps.setInt(5, tel);
            ps.setString(6, address);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void deleteCustomer(Customer cs) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, cs.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void deleteCustomerByID(int id) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
