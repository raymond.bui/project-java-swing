/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Order;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class OrderDAO {

    Connection con = Connector.connect();

    public ArrayList listOrder() {
        String stm = "SELECT * FROM ORDER";
        ArrayList<Order> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Order or = new Order();
                or.setId(rs.getInt(1));
                or.setName(rs.getString(2));
                or.setTelephone(rs.getInt(3));
                or.setAddress(rs.getString(4));
                or.setDate(rs.getString(5));
                list.add(or);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Order getOrderByID(int id) {
        Order or = new Order();
        String stm = "SELECT * FROM ORDER WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                or.setId(rs.getInt(1));
                or.setName(rs.getString(2));
                or.setTelephone(rs.getInt(3));
                or.setAddress(rs.getString(4));
                or.setDate(rs.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return or;
    }

    public Order addOrder(Order or) {
        String stm = "INSERT INTO ORDER(NAME, TELEPHONE, ADDRESS, DATE) VALUES(?, ?, ?, ?) SELECT @@IDENTITY asLastID";
        int newId = -1;
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, or.getName());
            ps.setInt(2, or.getTelephone());
            ps.setString(3, or.getAddress());
            ps.setString(4, or.getDate());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                newId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        Order newOrder = getOrderByID(newId);
        return newOrder;
    }
    
    public void updateOrder(Order or, String name, int tel, String address, String date) {
        String stm = "UPDATE ORDER SET NAME = ?, TELEPHONE = ?, ADDRESS = ?, DATE = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, tel);
            ps.setString(3, address);
            ps.setString(4, date);
            ps.setInt(5, or.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateOrderByID(int id, String name, int tel, String address, String date) {
        String stm = "UPDATE CUSTOMER SET USERNAME = ?, PASSWORD = ?, RANK = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, tel);
            ps.setString(3, address);
            ps.setString(4, date);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteOrder(Order or) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, or.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteOrderByID(int id) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
