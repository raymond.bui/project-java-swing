/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Order;
import Entities.OrderDetail;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class OrderDetailDAO {

    Connection con = Connector.connect();
    
    public ArrayList getOrderDetailByOrder(Order or) {
        ArrayList<OrderDetail> list = new ArrayList();
        String stm = "SELECT * FROM ORDERDETAIL WHERE ORDERID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, or.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDetail od = new OrderDetail();
                od.setOrder(new OrderDAO().getOrderByID(rs.getInt(1)));
                od.setProduct(new ProductDAO().getProductByID(rs.getInt(2)));
                od.setQuantity(rs.getInt(3));
                list.add(od);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }
    
    public void addOrderDetail(OrderDetail od) {
        String stm = "INSERT INTO ORDERDETAIL(ORDERID, PRODUCTID, QUANTITY) VALUES(?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, od.getOrder().getId());
            ps.setInt(2, od.getProduct().getId());
            ps.setInt(3, od.getQuantity());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateOrderDetail(OrderDetail od, int qtt) {
        String stm = "UPDATE ORDERDETAIL SET QUANTITY = ? WHERE ORDERID = ? AND PRODUCTID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, qtt);
            ps.setInt(2, od.getOrder().getId());
            ps.setInt(3, od.getProduct().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteOrderDetail(OrderDetail od) {
        String stm = "DELETE FROM ORDERDETAIL WHERE ORDERID = ? AND PRODUCTID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, od.getOrder().getId());
            ps.setInt(2, od.getProduct().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteOrderDetailByOrder(Order or) {
        String stm = "DELETE FROM ORDERDETAIL WHERE ORDERID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, or.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
