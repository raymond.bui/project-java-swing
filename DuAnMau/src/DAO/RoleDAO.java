/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Role;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Simon
 */
public class RoleDAO {

    Connection con = Connector.connect();

    public Role getRoleByID(int id) {
        Role ro = new Role();
        String stm = "SELECT * FROM ROLE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ro.setId(rs.getInt(1));
                ro.setPosition(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return ro;
    }
}
