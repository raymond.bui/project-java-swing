/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Rotation;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class RotationDAO {

    Connection con = Connector.connect();

    public ArrayList listRotation() {
        String stm = "SELECT * FROM ROTATION";
        ArrayList<Rotation> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Rotation rt = new Rotation();
                rt.setId(rs.getInt(1));
                rt.setProduct(new ProductDAO().getProductByID(rs.getInt(2)));
                rt.setQuantity(rs.getInt(3));
                rt.setDate(rs.getString(4));
                rt.setType(new RotationTypeDAO().getRotationTypeByID(rs.getInt(5)));
                list.add(rt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Rotation getRotationByID(int id) {
        Rotation rt = new Rotation();
        String stm = "SELECT * FROM ROTATION WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rt.setId(rs.getInt(1));
                rt.setProduct(new ProductDAO().getProductByID(rs.getInt(2)));
                rt.setQuantity(rs.getInt(3));
                rt.setDate(rs.getString(4));
                rt.setType(new RotationTypeDAO().getRotationTypeByID(rs.getInt(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return rt;
    }

    public void addRotation(Rotation rt) {
        String stm = "INSERT INTO ROTATION(PRODUCTID, QUANTITY, DATE, ROTATION) VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, rt.getProduct().getId());
            ps.setInt(2, rt.getQuantity());
            ps.setString(3, rt.getDate());
            ps.setInt(4, rt.getType().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateRotation(Rotation rt, int pid, int qtt, String date, int rtt) {
        String stm = "UPDATE ROTATION SET PRODUCTID = ?, QUANTITY = ?, DATE = ?, ROTATION = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, pid);
            ps.setInt(2, qtt);
            ps.setString(3, date);
            ps.setInt(4, rtt);
            ps.setInt(5, rt.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateRotationByID(int id, int pid, int qtt, String date, int rtt) {
        String stm = "UPDATE ROTATION SET PRODUCTID = ?, QUANTITY = ?, DATE = ?, ROTATION = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, pid);
            ps.setInt(2, qtt);
            ps.setString(3, date);
            ps.setInt(4, rtt);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteRotation(Rotation rt) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, rt.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteRotationByID(int id) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
