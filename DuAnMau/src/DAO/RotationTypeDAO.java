/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.RotationType;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Simon
 */
public class RotationTypeDAO {

    Connection con = Connector.connect();

    public RotationType getRotationTypeByID(int id) {
        RotationType rt = new RotationType();
        String stm = "SELECT * FROM ROTATIONTYPE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rt.setId(rs.getInt(1));
                rt.setType(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return rt;
    }
}
