/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Simon
 */
public class BillDetail {

    private Bill bill;
    private Product product;
    private int quantity;

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BillDetail() {

    }

    public BillDetail(Bill bill, Product product, int quantity) {
        this.bill = bill;
        this.product = product;
        this.quantity = quantity;
    }

}
