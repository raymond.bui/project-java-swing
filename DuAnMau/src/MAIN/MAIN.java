/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MAIN;

import Controller.Controller;
import Entities.ACCOUNT;
import View.Inside;
import View.Inside1;
import View.Inside2;
import View.Loading;
import View.Login;

/**
 *
 * @author raymo
 */
public class MAIN {

    public static void main(String[] args) {
        Controller controller = new Controller();
        Entities.info info = new Entities.info();
        Entities.ACCOUNT account = new ACCOUNT();
        controller.setInfo(info);
        controller.setAccount(account);
        Loading loading = new Loading(controller);
        View.Login login = new Login(controller);
        try {
            View.Inside inside = new Inside(controller);
            controller.setInside(inside);
            View.Inside1 inside1 = new Inside1(controller);
            controller.setInside1(inside1);
            View.Inside2 inside2 = new Inside2(controller);
            controller.setInside2(inside2);
        } catch (Exception e) {
            System.out.println("Chưa đăng nhập");
        }

        controller.setLoading(loading);
        controller.setLogin(login);
        

        controller.runloading();
    }
}
