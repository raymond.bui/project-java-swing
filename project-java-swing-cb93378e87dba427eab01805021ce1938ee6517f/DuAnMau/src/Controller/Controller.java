/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entities.ACCOUNT;
import Entities.info;
import View.Inside;
import View.Loading;
import View.Login;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author raymo
 */
public class Controller extends DAO.DAO {

    private String Username;
    private String Password;
    private static ACCOUNT account;
    private static info info;
    private static View.Loading loading;
    private static View.Login login;
    private static View.Inside inside;
    
    public static Loading getLoading() {
        return loading;
    }
    
    public static void setLoading(Loading loading) {
        Controller.loading = loading;
    }
    
    public static Login getLogin() {
        return login;
    }
    
    public static void setLogin(Login login) {
        Controller.login = login;
    }
    
    public static Inside getInside() {
        return inside;
    }
    
    public static void setInside(Inside inside) {
        Controller.inside = inside;
    }
    
    public info getInfo() {
        return info;
    }
    
    public void setInfo(info info) {
        this.info = info;
    }
    private String message;
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public ACCOUNT getAccount() {
        return account;
    }
    
    public void setAccount(ACCOUNT account) {
        this.account = account;
    }
    
    public void setPassword(String Password) {
        this.Password = Password;
    }
    
    public String getUsername() {
        return Username;
    }
    
    public void setUsername(String Username) {
        this.Username = Username;
    }

    public void checklogin() {
        
        String sql = "Select * from ACCOUNT where USERNAME=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, Username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                account.setUSERNAME(rs.getString("USERNAME"));
                account.setROLE(rs.getInt("ROLE"));
                account.setPASSWORD(rs.getString("PASSWORD"));

                //
                if (Password.equals(account.getPASSWORD())) {
                    
                    System.out.println(Username);
                    message = "Login Success";
                    
                } else {
                    message = "Login Failed";
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            
        }
    }

    public void loginSuccess() {
        loading.setVisible(false);
        login.setVisible(false);
        System.out.println(account.getUSERNAME());
        inside.setAccount(account);
        inside.setVisible(true);
    }

    public void runloading() {
        loading.setVisible(true);
    }

    public void runlogin() {
        loading.setVisible(false);
        login.setVisible(true);
    }
}
