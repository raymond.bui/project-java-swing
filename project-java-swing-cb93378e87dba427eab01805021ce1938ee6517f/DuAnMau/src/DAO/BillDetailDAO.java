/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Bill;
import Entities.BillDetail;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class BillDetailDAO {

    Connection con = Connector.connect();
    
    public ArrayList getBillDetailByBill(Bill or) {
        ArrayList<BillDetail> list = new ArrayList();
        String stm = "SELECT * FROM BILLDETAIL WHERE BILLID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, or.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BillDetail bd = new BillDetail();
                bd.setBill(new BillDAO().getBillByID(rs.getInt(1)));
                bd.setProduct(new ProductDAO().getProductByID(rs.getInt(2)));
                bd.setQuantity(rs.getInt(3));
                list.add(bd);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }
    
    public void addBillDetail(BillDetail bd) {
        String stm = "INSERT INTO BILLDETAIL(BILLID, PRODUCTID, QUANTITY) VALUES(?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, bd.getBill().getId());
            ps.setInt(2, bd.getProduct().getId());
            ps.setInt(3, bd.getQuantity());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateBillDetail(BillDetail bd, int qtt) {
        String stm = "UPDATE BILLDETAIL SET QUANTITY = ? WHERE BILLID = ? AND PRODUCTID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, qtt);
            ps.setInt(2, bd.getBill().getId());
            ps.setInt(3, bd.getProduct().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteBillDetail(BillDetail bd) {
        String stm = "DELETE FROM BILLDETAIL WHERE BILLID = ? AND PRODUCTID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, bd.getBill().getId());
            ps.setInt(2, bd.getProduct().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteBillDetailByBill(Bill or) {
        String stm = "DELETE FROM BILLDETAIL WHERE BILLID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, or.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
