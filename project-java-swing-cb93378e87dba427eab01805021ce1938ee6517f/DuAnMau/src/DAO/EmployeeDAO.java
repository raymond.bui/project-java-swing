/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.Connector;
import Entities.Employee;
import Entities.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class EmployeeDAO {

    Connection con = Connector.connect();

    public ArrayList listEmployee() {
        String stm = "SELECT * FROM EMPLOYEE";
        ArrayList<Employee> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Employee em = new Employee();
                em.setId(rs.getInt(1));
                em.setUsername(rs.getString(2));
                em.setPassword(rs.getString(3));
                em.setRole(new RoleDAO().getRoleByID(rs.getInt(4)));
                em.setName(rs.getString(5));
                em.setTelephone(rs.getInt(6));
                em.setAddress(rs.getString(7));
                list.add(em);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Employee getEmpolyeeByID(int id) {
        Employee em = new Employee();
        String stm = "SELECT * FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                em.setId(rs.getInt(1));
                em.setUsername(rs.getString(2));
                em.setPassword(rs.getString(3));
                em.setRole(new RoleDAO().getRoleByID(rs.getInt(4)));
                em.setName(rs.getString(5));
                em.setTelephone(rs.getInt(6));
                em.setAddress(rs.getString(7));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return em;
    }

    public void addEmployee(Employee em) {
        String stm = "INSERT INTO EMPLOYEE(USERNAME, PASSWORD, ROLE, NAME, TELEPHONE, ADDRESS) VALUES(?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, em.getUsername());
            ps.setString(2, em.getPassword());
            ps.setInt(3, em.getRole().getId());
            ps.setString(4, em.getName());
            ps.setInt(5, em.getTelephone());
            ps.setString(6, em.getAddress());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateEmployee (Employee em, String un, String pw, int roid, String name, int tel, String address) {
        String stm = "UPDATE EMPLOYEE SET USERNAME = ?, PASSWORD = ?, ROLE = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, un);
            ps.setString(2, pw);
            ps.setInt(3, roid);
            ps.setString(4, name);
            ps.setInt(5, tel);
            ps.setString(6, address);
            ps.setInt(7, em.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void updateEmployeeByID(int id, String un, String pw, int roid, String name, int tel, String address) {
        String stm = "UPDATE EMPLOYEE SET USERNAME = ?, PASSWORD = ?, ROLE = ?, NAME = ?, TELEPHONE = ?, ADDRESS = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, un);
            ps.setString(2, pw);
            ps.setInt(3, roid);
            ps.setString(4, name);
            ps.setInt(5, tel);
            ps.setString(6, address);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteEmployee(Employee em) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, em.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    public void deleteEmployeeByID(int id) {
        String stm = "DELETE FROM EMPLOYEE WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
