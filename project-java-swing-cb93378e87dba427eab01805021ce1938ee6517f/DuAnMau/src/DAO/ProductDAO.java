/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Product;
import Helper.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class ProductDAO {

    Connection con = Connector.connect();

    public ArrayList listProduct() {
        String stm = "SELECT * FROM PRODUCT";
        ArrayList<Product> list = new ArrayList();
        try {
            ResultSet rs = con.createStatement().executeQuery(stm);
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                p.setPrice(rs.getInt(3));
                p.setCategory(new CategoryDAO().getCategoryByID(rs.getInt(4)));
                p.setQuantity(rs.getInt(5));
                list.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public Product getProductByID(int id) {
        Product p = new Product();
        String stm = "SELECT * FROM PRODUCT WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                p.setPrice(rs.getInt(3));
                p.setCategory(new CategoryDAO().getCategoryByID(rs.getInt(4)));
                p.setQuantity(rs.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return p;
    }

    public void addProduct(Product p) {
        String stm = "INSERT INTO PRODUCT(NAME, PRICE, CATEGORY, QUANTITY) VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, p.getName());
            ps.setInt(2, p.getPrice());
            ps.setInt(3, p.getCategory().getId());
            ps.setInt(4, p.getQuantity());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void updateProduct(Product p, String name, int price, int category, int qtt) {
        String stm = "UPDATE PRODUCT SET NAME = ?, PRICE = ?, CATEGORY = ?, QUANTITY = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, price);
            ps.setInt(3, category);
            ps.setInt(4, qtt);
            ps.setInt(5, p.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void updateProductByID(int id, String name, int price, int category, int qtt) {
        String stm = "UPDATE PRODUCT SET NAME = ?, PRICE = ?, CATEGORY = ?, QUANTITY = ? WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setString(1, name);
            ps.setInt(2, price);
            ps.setInt(3, category);
            ps.setInt(4, qtt);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void deleteProduct(Product em) {
        String stm = "DELETE FROM PRODUCT WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, em.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public void deleteProductByID(int id) {
        String stm = "DELETE FROM PRODUCT WHERE ID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(stm);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
}
