/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Simon
 */
public class Rotation {

    private int id, quantity;
    private String date;
    private Product product;
    private RotationType type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public RotationType getType() {
        return type;
    }

    public void setType(RotationType type) {
        this.type = type;
    }

    public Rotation() {

    }

    public Rotation(int id, int quantity, String date, Product product, RotationType type) {
        this.id = id;
        this.quantity = quantity;
        this.date = date;
        this.product = product;
        this.type = type;
    }

}
